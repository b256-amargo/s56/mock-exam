function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    function isLetter(letterVar) {
        return letterVar.length === 1 && letterVar.match(/[a-z]/i);
    }

    if(isLetter(letter)){
        for(let index = 0; index < sentence.length; index++) {
            if(sentence[index] === letter){
                result += 1;
            }
        }
        return result;
    }
    else{
        return undefined;
    };
};


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    let result = true;
    let array = [];

    array.push(text[0]);

    for(let index = 1; index < text.length; index++) {

        let x = text[index].toLowerCase();

        if(array.includes(x)){
            result = false;
            break;
        }
        else{
            array.push(text[index].toLowerCase());
        }
    }
    return result;
};

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    let resultPrice;

    if(age < 13){
        return undefined;
    }
    else if((age >= 13 && age <=21) || age > 64){
        resultPrice = price * 0.8;
    }
    else if(age >= 22 && age <=64){
        resultPrice = price;
    }
    else{
        return undefined;
    }

    return resultPrice.toFixed(2);
};

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    let noStocksArray = [];

    for(let index = 0; index < items.length; index++) {

        let x = items[index];

        if(x.stocks === 0 && !noStocksArray.includes(x.category)){
            noStocksArray.push(x.category);
        };
    };

    return noStocksArray;
};

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    let flyingVoters = [];

    for(let index = 0; index < candidateA.length; index++) {

        let x = candidateA[index];

        if(candidateB.includes(x)){
            flyingVoters.push(x);
        };
    };

    return flyingVoters;
};

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};

